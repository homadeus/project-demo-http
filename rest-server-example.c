#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "contiki.h"
#include "contiki-net.h"
#include "rest.h"

#include <twi/twi.h>
#include <homadeus/utils/hex.h>
#include <homadeus/processes/relay.h>
#include <homadeus/devices/78M6610.h>

#include <homadeus/boards/board.h>
#include <homadeus/bootloader/bootloader.h>

#include <avr/wdt.h>
char temp[512];

#define DEBUG 1
#include <homadeus/utils/debug.h>


int add_value_to_string(char* str, uint8_t reg, char* value_name, uint8_t len, int max, uint8_t last) {
    char dest[len * 2 + 1];
    PRINTF("Reading: %s : ", value_name);
    uint8_t data[len];
    int rc = twi_writeTo(MAXIM_78M6610_TWI_ADDR, &reg, 1, 1);
    if (rc) {
      PRINTF(" [error writing to 78M6610: %d] ", rc);
    }
    rc = twi_readFrom(MAXIM_78M6610_TWI_ADDR, data, len);
    bin2hex(dest, data, len);
    dest[len*2] = 0;
    PRINTF("%s (%d of %d read)\n", dest, rc, len);
    if (last)
      return snprintf(str, max, "\"%s\":\"%s\"", value_name, dest);
    else
      return snprintf(str, max, "\"%s\":\"%s\",", value_name, dest);
}

#define ADD_VALUE(counter, value, length, last) \
  do { counter += add_value_to_string(temp + counter, MAXIM_78M6610_##value, #value, length, sizeof(temp) - counter - 1, last); } while(0)


enum ds7505_registers {
    P_TEMP = 0x0, // temperature
    P_CONF = 0x1, // configuration
    P_THYST = 0x2, // Thyst
    P_TOS = 0x3, // Tos
};

enum ds7505_resolutions {
  RES_09 = 0x0, /*!<  9 bit res */
  RES_10 = 0x1, /*!< 10 bit res */
  RES_11 = 0x2, /*!< 11 bit res */
  RES_12 = 0x3, /*!< 12 bit res */
};

RESOURCE(temperature, METHOD_GET, "temperature");
void temperature_handler(REQUEST* request, RESPONSE* response)
{
  const uint8_t a0 = 0, a1 = 0, a2 = 0;
  const uint8_t resolution = (RES_12 << 5);
  const uint8_t i2c_addr = 0x48 | (a2 & 0x1) << 2 | (a1 & 0x1) << 1 | (a0 & 0x1);
  uint8_t set_config[] = { P_CONF,  resolution };
  uint8_t get_temp[] = { P_TEMP };
  uint8_t temp[2];

  twi_writeTo(i2c_addr, set_config, sizeof(set_config), 1);
  twi_writeTo(i2c_addr, get_temp, sizeof(get_temp), 1);
  twi_readFrom(i2c_addr, temp, sizeof(temp));

  double s = 1.0, temperature = 0.0;
  if ((temp[0] & 0x80) == 0x80) {
    s = -1.0;
    temp[0] &= 0x7f;
  }
  else {
    s = 1.0;
  }

  temperature = s * 0.5 * ((temp[1] & 0x80 )>> 7) + 0.25 * ((temp[1] & 0x40 )>> 6)+ 0.125 * ((temp[1] & 0x20 )>> 5) + 0.0625 * ((temp[1] & 0x10 )>> 4) + (float) temp[0];

  static char buf[10];
  int rc = snprintf(buf, sizeof(buf), "%0.4f", temperature);

  rest_set_header_content_type(response, TEXT_PLAIN);
  rest_set_response_payload(response, (uint8_t *) buf, rc);

}

/* Resources are defined by RESOURCE macro, signature: resource name, the http methods it handles and its url*/
RESOURCE(helloworld, METHOD_GET, "helloworld");
void helloworld_handler(REQUEST* request, RESPONSE* response)
{

  int count = 0;
  count += snprintf(temp, sizeof(temp) - count - 1, "{");
  ADD_VALUE(count, FREQUENCY, 3, 0);
  ADD_VALUE(count, P_SCALE, 3, 0);
  ADD_VALUE(count, P, 3, 0);
  ADD_VALUE(count, P_AVG, 3, 0);
  ADD_VALUE(count, FW_VERSION, 3, 0);
  ADD_VALUE(count, TEMPERATURE, 3, 0);
  ADD_VALUE(count, FREQUENCY_UNSCALED, 3, 1);
  count += snprintf(temp + count, sizeof(temp) - count - 1, "}");

  /*
  debug_register(P_SCALE, 3);
  debug_register(P, 3);
  debug_register(P_AVG, 3);
  debug_register(FW_VERSION, 3);
  debug_register(T_SCALE, 3);
  debug_register(TEMPERATURE, 3);
  debug_register(F_SCALE, 3);
  debug_register(FREQUENCY, 3);
  debug_register(FREQUENCY_UNSCALED, 3);
  */
  wdt_disable();
  /*
  maxim_78M6610_debug_version(temp);
  TEMPERATURE_type t = maxim_78M6610_read_temp(ttemp);
  sprintf(temp,"%s/%s/%g: Hello World!\n", temp, ttemp, t);
  */

  rest_set_header_content_type(response, TEXT_PLAIN);
  rest_set_response_payload(response, (uint8_t*)temp, strlen(temp));
}

/* Resources are defined by RESOURCE macro, signature: resource name, the http methods it handles and its url*/
RESOURCE(relay_on, METHOD_GET, "relay/on");
void relay_on_handler(REQUEST* request, RESPONSE* response)
{
  static uint8_t relay_state = 1;
  process_post(&HOMADEUS_RELAY_PROCESS, HOMADEUS_RELAY_EVENT, &relay_state);
  rest_set_header_content_type(response, TEXT_PLAIN);
  rest_set_response_payload(response, NULL, 0);
}

RESOURCE(relay_off, METHOD_GET, "relay/off");
void relay_off_handler(REQUEST* request, RESPONSE* response)
{
  static uint8_t relay_state = 0;
  process_post(&HOMADEUS_RELAY_PROCESS, HOMADEUS_RELAY_EVENT, &relay_state);
  rest_set_header_content_type(response, TEXT_PLAIN);
  rest_set_response_payload(response, NULL, 0);
}

RESOURCE(dump, METHOD_GET, "dump");
void dump_handler(REQUEST* request, RESPONSE* response)
{

  if (spi_flash) {
    PRINTF("Dumping\r\n");
    bootloader_firmware_dump_to_recovery(spi_flash);
    PRINTF("Dumped\r\n");
  } else {
    PRINTF("Unable to open flash chip\r\n");
  }
  rest_set_header_content_type(response, TEXT_PLAIN);
  rest_set_response_payload(response, NULL, 0);
}

PROCESS(rest_server_example, "Rest Server Example");
AUTOSTART_PROCESSES(&rest_server_example, &HOMADEUS_RELAY_PROCESS);

PROCESS_THREAD(rest_server_example, ev, data)
{
  PROCESS_BEGIN();

  twi_init();

  PRINTF("HTTP Server\n");

  rest_init();

  rest_activate_resource(&resource_temperature);
  rest_activate_resource(&resource_helloworld);
  rest_activate_resource(&resource_relay_on);
  rest_activate_resource(&resource_relay_off);
  rest_activate_resource(&resource_dump);

  PROCESS_END();
}
