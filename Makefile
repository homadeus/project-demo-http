all: rest-server-example

avrdude:
	make rest-server-example.$(TARGET).u AVRDUDE_PROGRAMMER="-c usbasp" AVRDUDE_PORT="" AVRDUDE_OPTIONS="-V"

ifndef TARGET
TARGET=avr-homadeus
endif

CONTIKI=../../os/contiki

UIP_CONF_IPV6=1
UIP_CONF_IPV6_RPL=1

CFLAGS += -DPROJECT_CONF_H=\"project-conf.h\"
CFLAGS += -DWITH_HTTP
CFLAGS += -DUIP_CONF_IPV6=1
CFLAGS += -DUIP_CONF_IPV6_RPL=1

LDFLAGS += -Wl,-u,vfprintf -lprintf_flt -lm

APPS += rest-http

MY_APPS += bootloader_control spi_driver tiki_utils twi_driver twi_78M6610 spi_flash

APPS += $(MY_APPS) hmd_relay_process
APPDIRS += $(join $(addprefix ../../apps/,$(MY_APPS)), )

include $(CONTIKI)/Makefile.include
